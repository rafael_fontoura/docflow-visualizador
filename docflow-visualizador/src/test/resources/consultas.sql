﻿select documento.id_documento * 1000 nivel	
	, documento.nm_documento nome
	, documento.dt_criacao dt_criacao
	, arquivo.id_arquivo id_arquivo
	, arquivo.nm_arquivo nomearquivo
from tb_documento documento
left join tb_arquivo arquivo on arquivo.id_arquivo = documento.id_arquivo
union
select documento.id_documento * 1000 + anexo.id_anexo nivel
	, anexo.nm_anexo
	, anexo.dt_criacao dt_criacao
	, arquivo.id_arquivo id_arquivo
	, arquivo.nm_arquivo nomearquivo
from tb_anexo anexo
join tb_documento documento on documento.id_documento = anexo.id_documento
join tb_arquivo arquivo on arquivo.id_arquivo = anexo.id_arquivo;

---------------------------------------------
