package br.com.dataeasy.docflow.visualizador.test;

import java.lang.reflect.ParameterizedType;

import org.powermock.api.easymock.PowerMock;
import org.powermock.reflect.Whitebox;

public class DocflowBaseTest<T> {

	private T classeDeTeste;
	
	public void setUp() {
		PowerMock.resetAll();
	}

	@SuppressWarnings("unchecked")
	protected T getClasseDeTeste() {
		if (this.classeDeTeste == null) {
			Class<T> classe = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass())
					.getActualTypeArguments()[0];
			try {
				this.classeDeTeste = classe.newInstance();
			} catch (Exception e) {
				throw new RuntimeException("Erro instanciando " + classe + ".", e);
			}
		}
		return this.classeDeTeste;
	}

	protected void setClasseDeTeste(T classeDeTeste) {
		this.classeDeTeste = classeDeTeste;
	}

	protected <M> M adicionarMock(Class<M> classeMock) {
		M mock = PowerMock.createNiceMock(classeMock);
		Whitebox.setInternalState(getClasseDeTeste(), classeMock, mock);
		return mock;
	}
}
