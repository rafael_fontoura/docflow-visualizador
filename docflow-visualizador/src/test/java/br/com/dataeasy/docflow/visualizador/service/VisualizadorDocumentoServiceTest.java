package br.com.dataeasy.docflow.visualizador.service;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;

import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;
import br.com.dataeasy.docflow.visualizador.test.ServiceBaseTest;
import br.com.dataeasy.docflow.visualizador.test.mock.ArquivoMock;

public class VisualizadorDocumentoServiceTest extends ServiceBaseTest<VisualizadorDocumentoService> {
	
	@Test
	public void testGravarDocumentoComEspacosEAcentos() throws IOException {
		String nomeOriginal = "Nome De Arquívo Sem Espáços.pdf";
		Arquivo arquivo = ArquivoMock.criarArquivo(1, nomeOriginal);
		
		PowerMock.replayAll();
		File file = getService().gravarDocumentoEmDisco(arquivo);
		Assert.assertEquals("Nome%20De%20Arquivo%20Sem%20Espacos.pdf", file.getName());
		PowerMock.verifyAll();
	}

	@Test
	public void testGravarDocumentoEmDisco() {
		fail("Not yet implemented");
	}

	@Test
	public void testIsMesmoArquivo() {
		fail("Not yet implemented");
	}

	@Test
	public void testRecuperarDiretorioDaSessaoHttpSession() {
		fail("Not yet implemented");
	}

	@Test
	public void testRecuperarDiretorioDaSessao() {
		fail("Not yet implemented");
	}

	@Test
	public void testDocumentoExisteEmDisco() {
		fail("Not yet implemented");
	}

	@Test
	public void testFormatarNomeDocumento() {
		fail("Not yet implemented");
	}

}
