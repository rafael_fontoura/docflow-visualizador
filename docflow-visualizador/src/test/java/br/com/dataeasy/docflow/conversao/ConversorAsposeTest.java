package br.com.dataeasy.docflow.conversao;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;

import br.com.dataeasy.docflow.visualizador.conversao.ConversorAsposeDeFile;

public class ConversorAsposeTest {

	@Test
	public void testConverterDocXParaPDF() throws URISyntaxException {
		String nomeArquivo = "/arquivos/respondus-docx-sample-file.docx";
		testConverterParaPDF(nomeArquivo);
	}

	@Test
	public void testConverterDocParaPDF() throws URISyntaxException {
		String nomeArquivo = "/arquivos/darknet5.doc";
		testConverterParaPDF(nomeArquivo);
	}
	
	@Test
	public void testConverterOdtParaPDF() throws URISyntaxException {
		String nomeArquivo = "/arquivos/Creating_large_documents_with_OOo.odt";
		testConverterParaPDF(nomeArquivo);
	}

	@Test
	public void testConverterXlsParaPDF() throws URISyntaxException {
		String nomeArquivo = "/arquivos/QTL_Sample_data.xls";
		testConverterParaPDF(nomeArquivo);
	}

	@Test
	public void testConverterXlsxParaPDF() throws URISyntaxException {
		String nomeArquivo = "/arquivos/Sampleformslist.xlsx";
		testConverterParaPDF(nomeArquivo);
	}
	
	@Test
	public void testConverterOdsParaPDF() throws URISyntaxException {
		String nomeArquivo = "/arquivos/consolidation2.ods";
		testConverterParaPDF(nomeArquivo);
	}

	@Test
	public void testConverterPptParaPDF() throws URISyntaxException {
		String nomeArquivo = "/arquivos/Sample.ppt";
		testConverterParaPDF(nomeArquivo);
	}

	@Test
	public void testConverterPptxParaPDF() throws URISyntaxException {
		String nomeArquivo = "/arquivos/Performance_Out.pptx";
		testConverterParaPDF(nomeArquivo);
	}
	
	@Test
	public void testConverterOdpParaPDF() throws URISyntaxException {
		String nomeArquivo = "/arquivos/friday_1484.odp";
		testConverterParaPDF(nomeArquivo);
	}

	private void testConverterParaPDF(String nomeArquivo) throws URISyntaxException {
		apagarArquivoPDFCasoExista(nomeArquivo);

		File arquivo = new File(getClass().getResource(nomeArquivo).toURI());
		new ConversorAsposeDeFile().converterParaPDF(arquivo);
		Assert.assertNotNull(getArquivoPDF(nomeArquivo));
	}

	private void apagarArquivoPDFCasoExista(String nomeArquivo) throws URISyntaxException {
		File filePDF = getArquivoPDF(nomeArquivo);
		if (filePDF != null) {
			filePDF.delete();
		}
	}

	private File getArquivoPDF(String nomeArquivo) throws URISyntaxException {
		String nomeArquivoPDF = nomeArquivo.substring(0, nomeArquivo.lastIndexOf('.')) + ".pdf";
		File file = null;
		URL url = getClass().getResource(nomeArquivoPDF);
		if (url != null) {
			file = new File(url.toURI());
		}
		return file;
	}
}