package br.com.dataeasy.docflow.visualizador.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

public class FileUtilTest {
	private static final Logger LOG = Logger.getLogger(FileUtilTest.class);

	@Test
	public void testCalcularMD5Checksum1() throws Exception {
		testaCalcularMD5Checksum("/arquivos/install.log", "b42f7363d801545bac929a7c30122d2e");
	}

	@Test
	public void testCalcularMD5Checksum2() throws Exception {
		testaCalcularMD5Checksum("/arquivos/Performance_Out.pptx", "8ab08c78cc01820ed0787a9b6e47fe2d");
	}

	private void testaCalcularMD5Checksum(String caminhoArquivo, String checksum) throws URISyntaxException,
			FileNotFoundException, IOException {
		URI uri = getClass().getResource(caminhoArquivo).toURI();
		InputStream is = new FileInputStream(new File(uri));
		long tempoInicio = System.currentTimeMillis();
		String resultado = FileUtil.calcularMD5Checksum(is);
		long tempoFim = System.currentTimeMillis();
		LOG.info((tempoFim - tempoInicio) + " milissegundos para a operação.");

		Assert.assertEquals(checksum, resultado);
	}
}
