package br.com.dataeasy.docflow.visualizador.service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.api.easymock.PowerMock;

import br.com.dataeasy.docflow.visualizador.conversao.ConversorAsposeDeInputStream;
import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;
import br.com.dataeasy.docflow.visualizador.repository.ArquivoDAO;
import br.com.dataeasy.docflow.visualizador.test.ServiceBaseTest;
import br.com.dataeasy.docflow.visualizador.test.mock.ArquivoMock;

public class ArquivoServiceTest extends ServiceBaseTest<ArquivoService> {

	private ArquivoDAO arquivoDAO;
	private ConversorAsposeDeInputStream conversorAsposeDeInputStream;
	private VisualizadorDocumentoService visualizadorDocumentoService;

	@Before
	public void setUp() {
		super.setUp();
		arquivoDAO = adicionarMock(ArquivoDAO.class);
		conversorAsposeDeInputStream = adicionarMock(ConversorAsposeDeInputStream.class);
		visualizadorDocumentoService = adicionarMock(VisualizadorDocumentoService.class);
	}

	@Test
	public void testListar() {
		EasyMock.expect(arquivoDAO.listar()).andReturn(new ArrayList<Arquivo>());
		PowerMock.replayAll();

		List<Arquivo> resultado = getService().listar();
		Assert.assertNotNull(resultado);
		Assert.assertTrue(resultado.isEmpty());
		PowerMock.verifyAll();
	}

	@Test
	public void testRecuperarArquivoNaoVisualizavelENaoConversivel() {
		Arquivo arquivo = ArquivoMock.criarArquivo(1, "nomeDeArquivo.java");

		EasyMock.expect(arquivoDAO.obter(arquivo.getId())).andReturn(arquivo);
		EasyMock.expect(conversorAsposeDeInputStream.permiteConversao("java")).andReturn(false);

		PowerMock.replayAll();
		String nomeArquivo = getService().recuperarArquivoParaVisualizacao(arquivo.getId());
		Assert.assertNull(nomeArquivo);
		PowerMock.verifyAll();
	}

	@Test
	public void testRecuperarArquivoVisualizavelComNomeSemEspacos() throws IOException {
		String nomeOriginal = "NomeDeArquivoSemEspacos.pdf";
		Arquivo arquivo = ArquivoMock.criarArquivo(1, nomeOriginal);
		EasyMock.expect(arquivoDAO.obter(arquivo.getId())).andReturn(arquivo);
		EasyMock.expect(visualizadorDocumentoService.gravarDocumentoEmDisco(arquivo)).andReturn(new File(nomeOriginal));

		PowerMock.replayAll();
		String nomeArquivo = getService().recuperarArquivoParaVisualizacao(arquivo.getId());
		Assert.assertEquals(nomeOriginal, nomeArquivo);
		PowerMock.verifyAll();
	}
}
