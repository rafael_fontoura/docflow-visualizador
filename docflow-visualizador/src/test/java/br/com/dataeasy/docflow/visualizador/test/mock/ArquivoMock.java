package br.com.dataeasy.docflow.visualizador.test.mock;

import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;
import br.com.dataeasy.docflow.visualizador.dominio.ConteudoArquivo;

public class ArquivoMock {

	public static Arquivo criarArquivo(Integer id, String nome) {
		Arquivo arquivo = new Arquivo();
		arquivo.setId(id);
		arquivo.setNome(nome);
		arquivo.setConteudoArquivo(new ConteudoArquivo());
		arquivo.getConteudoArquivo().setConteudo(new byte[] {});
		return arquivo;
	}
}
