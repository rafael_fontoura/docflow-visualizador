package br.com.dataeasy.docflow.visualizador.listener;

import java.io.File;

import org.junit.Assert;
import org.junit.Test;

import com.atalasoft.licensing.LicenseValidator;

/**
 * <b>Title:</b> LicencaJoltImageContextListenerTest.java <br>
 * <b>Description:</b>  <br>
 * <b>Package:</b> br.com.dataeasy.docflow.visualizador.listener <br>
 * <b>Project:</b> docflow-visualizador <br>
 * <b>Company:</b> DataEasy Consultoria e Informática LTDA. <br>
 * 
 *    Copyright (c) 2014 DataEasy - Todos os direitos reservados.
 * 
 * @author Rafael
 * @version Revision: $ Date: 19/03/2014
 */
public class LicencaJoltImageContextListenerTest {

	@Test
	public void testLicencaPorString() {
		String licencaStr = "<?xml version='1.0' encoding='utf-8'?><License>  "
				+ "<Assembly>imaging.ImagingProduct</Assembly>  <Version>10.4</Version>  "
				+ "<SerialNumber>JAI2-49C3-9F4E-3B0B-2CE5</SerialNumber>  <Name>Rafael Fontoura | DataEasy</Name>  "
				+ "<MachineID>00:1F:3C:0A:E4:D1</MachineID>  <Flags />  <MachineName>RAFAEL-NB</MachineName>  "
				+ "<UserName>Rafael</UserName>  <Flag name='v2Signature' />  "
				+ "<Signature>omIUvhzgaTPG2Lc7/xnXS44yCs/uxBEiYCNai7E524HUMgTFwiK3jagQsMgyyINiHhafhz0SmuehGq7WrBpXF+i3G8"
				+ "7jt9fSFkjLC8KWhppl2UxVixhJlejhjut9xaWkMg+/LD1krDGPutw7bk2sHqX0nCMjSF8sFRlNHTCbvds=</Signature></License>";
		LicenseValidator
				.validateLicense(licencaStr);
	}
	
	@Test
	public void testLicencaPorArquivoLicenca() throws Exception {
		File file = new File(getClass().getClassLoader().getResource("JoltImage.imaging.lic").toURI());
		Assert.assertTrue(file.exists());
		LicenseValidator.validateLicense(file);
	}

}
