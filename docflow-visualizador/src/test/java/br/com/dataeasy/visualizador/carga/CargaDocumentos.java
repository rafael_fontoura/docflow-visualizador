package br.com.dataeasy.visualizador.carga;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;
import br.com.dataeasy.docflow.visualizador.dominio.ConteudoArquivo;
import br.com.dataeasy.docflow.visualizador.util.FileUtil;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:applicationContext-test.xml" })
@TransactionConfiguration(defaultRollback = false, transactionManager = "myTxManager")
@Transactional(readOnly = false)
public class CargaDocumentos {
	
	@PersistenceContext
	private EntityManager em;

	@Test
	public void inserirMassaDeDados() throws IOException {
		limparBaseDeDados();
		inserirArquivos(new File("c:\\temp\\arquivos"));
	}
	
	private void limparBaseDeDados() {
		em.createQuery("DELETE FROM br.com.dataeasy.docflow.visualizador.dominio.Anexo").executeUpdate();
		em.createQuery("DELETE FROM br.com.dataeasy.docflow.visualizador.dominio.Documento").executeUpdate();
		em.createQuery("DELETE FROM br.com.dataeasy.docflow.visualizador.dominio.Arquivo").executeUpdate();
		em.createQuery("DELETE FROM br.com.dataeasy.docflow.visualizador.dominio.Processo").executeUpdate();
	}

	private void inserirArquivos(File diretorioBase) throws FileNotFoundException, IOException {
		for (String nomeArquivo : diretorioBase.list()) {
			File file = new File(diretorioBase, nomeArquivo);
			InputStream is = new FileInputStream(file);

			Arquivo arquivo = new Arquivo();
			arquivo.setNome(nomeArquivo);
			byte[] conteudo = FileUtil.getByteArray(is);
		
			ConteudoArquivo conteudoArquivo = new ConteudoArquivo();
			conteudoArquivo.setConteudo(conteudo);
			arquivo.setConteudoArquivo(conteudoArquivo);
			em.persist(arquivo);
		}
	}
}