package br.com.dataeasy.docflow.visualizador.arvore;

import java.util.Enumeration;

import javax.swing.tree.TreeNode;

/**
 * <b>Title:</b> NoAnexo.java <br>
 * <b>Description:</b>Nó que representa um anexo de documento/processo na árvore de itens do visualizador de documentos.<br>
 * <b>Package:</b> br.com.dataeasy.docflow.visualizador.arvore <br>
 * <b>Project:</b> docflow-visualizador <br>
 * <b>Company:</b> DataEasy Consultoria e Informática LTDA. <br>
 * 
 * Copyright (c) 2014 DataEasy - Todos os direitos reservados.
 * 
 * @author rafael.fontoura
 * @version Revision: $ Date: 11/03/2014
 */
public class NoAnexo extends NoVisualizador {

	private static final long serialVersionUID = 8861743100362133043L;
	private NoVisualizador noPai;
	
	public NoAnexo() {
		super();
	}

	public NoAnexo(String nome) {
		super(nome);
	}

	/**
	 * @param childIndex
	 * @return
	 */
	@Override
	public TreeNode getChildAt(int childIndex) {
		return null;
	}

	/**
	 * @return
	 */
	@Override
	public int getChildCount() {
		return 0;
	}

	/**
	 * @return
	 */
	@Override
	public TreeNode getParent() {
		return noPai;
	}

	/**
	 * @param node
	 * @return
	 */
	@Override
	public int getIndex(TreeNode node) {
		return 0;
	}

	/**
	 * @return
	 */
	@Override
	public boolean getAllowsChildren() {
		return false;
	}

	/**
	 * @return
	 */
	@Override
	public boolean isLeaf() {
		return true;
	}

	@Override
	public Enumeration<TreeNode> children() {
		return null;
	}

	/**
	 * @return
	 */
	@Override
	public String getTipo() {
		return ANEXO;
	}

	public NoDocumento getDocumento() {
		if (DOCUMENTO.equals(noPai.getTipo())) {
			return (NoDocumento) noPai;
		}
		return null;
	}

	public NoProcesso getProcesso() {
		if (PROCESSO.equals(noPai.getTipo())) {
			return (NoProcesso) noPai;
		}
		return null;
	}

	public NoVisualizador getNoPai() {
		return noPai;
	}

	public void setNoPai(NoVisualizador noPai) {
		this.noPai = noPai;
	}
}