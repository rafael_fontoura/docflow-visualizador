package br.com.dataeasy.docflow.visualizador.arvore;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.tree.TreeNode;

import br.com.dataeasy.docflow.visualizador.dominio.Documento;

import com.google.common.collect.Iterators;

/**
 * <b>Title:</b> NoDocumento.java <br>
 * <b>Description:</b>  <br>
 * <b>Package:</b> br.com.dataeasy.docflow.visualizador.arvore <br>
 * <b>Project:</b> docflow-visualizador <br>
 * <b>Company:</b> DataEasy Consultoria e Informática LTDA. <br>
 * 
 *    Copyright (c) 2014 DataEasy - Todos os direitos reservados.
 * 
 * @author rafael.fontoura
 * @version Revision: $ Date: 11/03/2014
 */
public class NoDocumento extends NoVisualizador {
	
	private static final long serialVersionUID = 6857173923174233924L;
	private NoProcesso processo;
	private List<NoAnexo> anexos = new ArrayList<>();
	
	public NoDocumento() {
		super();
	}

	public NoDocumento(String nome) {
		super(nome);
	}

	/**
	 * @param childIndex
	 * @return
	 */
	@Override
	public TreeNode getChildAt(int childIndex) {
		return anexos.get(childIndex);
	}

	/**
	 * @return
	 */
	@Override
	public int getChildCount() {
		return anexos.size();
	}

	/**
	 * @return
	 */
	@Override
	public TreeNode getParent() {
		return processo;
	}

	/**
	 * @param node
	 * @return
	 */
	@Override
	public int getIndex(TreeNode node) {
		return anexos.indexOf(node);
	}

	/**
	 * @return
	 */
	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * @return
	 */
	@Override
	public boolean isLeaf() {
		return false;
	}

	/**
	 * @return
	 */
	@Override
	public Enumeration<NoAnexo> children() {
		return Iterators.asEnumeration(anexos.iterator());
	}

	@Override
	public String getTipo() {
		return DOCUMENTO;
	}
	
	public void adicionarAnexo(NoAnexo no) {
		anexos.add(no);
		no.setNoPai(this);
	}
	
	public void setProcesso(NoProcesso processo) {
		this.processo = processo;
	}

	public NoProcesso getProcesso() {
		return processo;
	}

	/**
	 * @param noVisualizador
	 */
	@Override
	public void setNoPai(NoVisualizador noVisualizador) {
		setProcesso((NoProcesso) noVisualizador);
	}
}