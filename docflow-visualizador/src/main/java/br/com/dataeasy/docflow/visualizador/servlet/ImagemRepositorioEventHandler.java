package br.com.dataeasy.docflow.visualizador.servlet;

import org.apache.log4j.Logger;

import com.atalasoft.imaging.AtalaImage;
import com.atalasoft.imaging.webcontrols.AnnotationDataRequestedEvent;
import com.atalasoft.imaging.webcontrols.AnnotationStreamWrittenEvent;
import com.atalasoft.imaging.webcontrols.DocumentInfoRequestedEvent;
import com.atalasoft.imaging.webcontrols.DocumentSavingEvent;
import com.atalasoft.imaging.webcontrols.DocumentStreamWrittenEvent;
import com.atalasoft.imaging.webcontrols.ImageRequestedEvent;
import com.atalasoft.imaging.webcontrols.WebDocumentViewerEventListener;

public class ImagemRepositorioEventHandler implements WebDocumentViewerEventListener {

	private Logger LOG = Logger.getLogger(ImagemRepositorioEventHandler.class);

	@Override
	public void annotationDataRequested(AnnotationDataRequestedEvent arg0) {
		// não faz nada
	}

	@Override
	public void annotationStreamWritten(AnnotationStreamWrittenEvent arg0) {
		// não faz nada

	}

	@Override
	public void documentInfoRequested(DocumentInfoRequestedEvent evt) {
		// evt.setFilePath("c:\\temp\\arquivos\\v43n1a1.tif");
		LOG.info("FilePath: " + evt.getFilePath());
	}

	@Override
	public void documentSaving(DocumentSavingEvent arg0) {
		// não faz nada
	}

	/**
	 * @param arg0
	 */
	@Override
	public void documentStreamWritten(DocumentStreamWrittenEvent evt) {
		// não faz nada
	}

	@Override
	public void imageRequested(ImageRequestedEvent evt) {
		// evt.setFilePath("c:\\temp\\arquivos\\v43n1a1.tif");

		LOG.info("FilePath: " + evt.getFilePath());
	}
}