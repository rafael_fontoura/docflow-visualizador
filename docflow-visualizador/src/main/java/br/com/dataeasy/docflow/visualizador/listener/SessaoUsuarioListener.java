package br.com.dataeasy.docflow.visualizador.listener;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import br.com.dataeasy.docflow.visualizador.service.VisualizadorDocumentoService;

public class SessaoUsuarioListener implements HttpSessionListener {
	private static Logger LOG = Logger.getLogger(SessaoUsuarioListener.class);
	private File diretorioDoUsuario;

	@Override
	public void sessionCreated(HttpSessionEvent se) {
		diretorioDoUsuario = new VisualizadorDocumentoService().recuperarDiretorioDaSessao(se.getSession());
		LOG.info("Sessão criada: " + se.getSession().getId());
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		if (diretorioDoUsuario.exists()) {
			try {
				FileUtils.deleteDirectory(diretorioDoUsuario);
			} catch (IOException e) {
				LOG.error("Problema ao excluir diretório " + diretorioDoUsuario + ".", e);
			}
		}
	}
}
