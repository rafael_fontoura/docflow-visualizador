package br.com.dataeasy.docflow.visualizador.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.atalasoft.imaging.webcontrols.WebDocumentRequestHandler;

@MultipartConfig
@WebServlet(name = "VisualizadorHandler", urlPatterns = { "*.view" })
public class DocFlowVisualizadorHandler extends WebDocumentRequestHandler {

	private static final long serialVersionUID = 2098895626217640629L;
	private static final Logger LOG = Logger.getLogger(DocFlowVisualizadorHandler.class);

	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			LOG.info("doPost sendo chamado");
			super.doPost(request, response);
		} catch (Exception e) {
			LOG.error(e.getMessage() + ": " + e.getLocalizedMessage(), e);
		}
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {
			LOG.info("doGet sendo chamado");
			addWebDocumentViewerListener(new ImagemRepositorioEventHandler());
			super.doGet(request, response);
		} catch (Exception e) {
			LOG.error(e.getClass().getSimpleName());
		}
	}
}
