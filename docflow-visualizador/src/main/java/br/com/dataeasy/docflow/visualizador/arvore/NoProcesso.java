package br.com.dataeasy.docflow.visualizador.arvore;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.swing.tree.TreeNode;

import com.google.common.collect.Iterators;

/**
 * <b>Title:</b> NoProcesso.java <br>
 * <b>Description:</b>Representa um processo na árvore de itens do visualizador de documentos. Pode ter como nós filhos
 * volumes e anexos (caso o processo não tenha volume).<br>
 * <b>Package:</b> br.com.dataeasy.docflow.visualizador.arvore <br>
 * <b>Project:</b> docflow-visualizador <br>
 * <b>Company:</b> DataEasy Consultoria e Informática LTDA. <br>
 * 
 * Copyright (c) 2014 DataEasy - Todos os direitos reservados.
 * 
 * @author rafael.fontoura
 * @version Revision: $ Date: 11/03/2014
 */
public class NoProcesso extends NoVisualizador {

	private static final long serialVersionUID = -7281847973817606612L;
	private List<NoVisualizador> itens = new ArrayList<>();
	
	public NoProcesso() {
		super();
	}

	public NoProcesso(String nome) {
		super(nome);
	}

	/**
	 * @param childIndex
	 * @return
	 */
	@Override
	public TreeNode getChildAt(int childIndex) {
		return itens.get(childIndex);
	}

	/**
	 * @return
	 */
	@Override
	public int getChildCount() {
		return itens.size();
	}

	/**
	 * @return
	 */
	@Override
	public TreeNode getParent() {
		return null;
	}

	/**
	 * @param node
	 * @return
	 */
	@Override
	public int getIndex(TreeNode node) {
		return itens.indexOf(node);
	}

	/**
	 * @return
	 */
	@Override
	public boolean getAllowsChildren() {
		return true;
	}

	/**
	 * @return
	 */
	@Override
	public boolean isLeaf() {
		return false;
	}

	/**
	 * @return
	 */
	@Override
	public Enumeration<NoVisualizador> children() {
		return Iterators.asEnumeration(itens.iterator());
	}

	/**
	 * @return
	 */
	@Override
	public String getTipo() {
		return PROCESSO;
	}
	
	public void adicionarItem(NoVisualizador no) {
		itens.add(no);
		no.setNoPai(this);
	}

	/**
	 * @param noVisualizador
	 */
	@Override
	public void setNoPai(NoVisualizador noVisualizador) {
		// não tem nó pai
	}
}