package br.com.dataeasy.docflow.visualizador.model;

import java.io.Serializable;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.springframework.stereotype.Component;

import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;
import br.com.dataeasy.docflow.visualizador.service.ArquivoService;
import br.com.dataeasy.docflow.visualizador.service.VisualizadorDocumentoService;

@Component
@ManagedBean(name = "arquivoBean")
@ViewScoped
public class ArquivoBean implements Serializable {

	private static final long serialVersionUID = -4671167362148634992L;
	private String nomeArquivo;

	@Resource
	private ArquivoService arquivoService;

	@Resource
	private VisualizadorDocumentoService visualizadorDocumentoService;

	public List<Arquivo> getArquivos() {
		return arquivoService.listar();
	}

	public void visualizarArquivo(Integer idArquivo) {
		try {
			nomeArquivo = arquivoService.recuperarArquivoParaVisualizacao(idArquivo);
		} catch (Throwable e) {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), null);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	public String getNomeArquivo() {
		return nomeArquivo;
	}

	public void setNomeArquivo(String nomeArquivo) {
		this.nomeArquivo = nomeArquivo;
	}
}