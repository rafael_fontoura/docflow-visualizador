package br.com.dataeasy.docflow.visualizador.model;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.richfaces.component.UITree;
import org.richfaces.event.TreeSelectionChangeEvent;
import org.richfaces.model.TreeNode;
import org.springframework.stereotype.Component;

import br.com.dataeasy.docflow.visualizador.arvore.NoAnexo;
import br.com.dataeasy.docflow.visualizador.arvore.NoDocumento;
import br.com.dataeasy.docflow.visualizador.arvore.NoProcesso;
import br.com.dataeasy.docflow.visualizador.arvore.NoVolume;

/**
 * <b>Title:</b> ArvoreProcessoBean.java <br>
 * <b>Description:</b> <br>
 * <b>Package:</b> br.com.dataeasy.docflow.visualizador.model <br>
 * <b>Project:</b> docflow-visualizador <br>
 * <b>Company:</b> DataEasy Consultoria e Informática LTDA. <br>
 * 
 * Copyright (c) 2014 DataEasy - Todos os direitos reservados.
 * 
 * @author rafael.fontoura
 * @version Revision: $ Date: 12/03/2014
 */
@Component
@ManagedBean(name = "arvoreProcessoBean")
@ViewScoped
public class ArvoreProcessoBean {

	private NoProcesso noProcesso;
	private TreeNode currentSelection;

	public NoProcesso getNoRaiz() {
		if (noProcesso == null) {
			carregarNoProcesso();
		}
		return noProcesso;
	}

	private void carregarNoProcesso() {
		NoDocumento documento2 = new NoDocumento("Documento 2");
		documento2.adicionarAnexo(new NoAnexo("Anexo 1 do Documento"));
		documento2.adicionarAnexo(new NoAnexo("Anexo 2 do Documento"));

		NoVolume volume1 = new NoVolume("Volume 1");
		volume1.adicionarItem(new NoAnexo("Anexo1 do Processo 1"));
		volume1.adicionarItem(new NoDocumento("Documento 1"));
		volume1.adicionarItem(documento2);

		NoDocumento documento4 = new NoDocumento("Documento 4");
		documento4.adicionarAnexo(new NoAnexo("Anexo 1 do Documento 4"));
		documento4.adicionarAnexo(new NoAnexo("Anexo 2 do Documento 4"));

		NoVolume volume2 = new NoVolume("Volume 2");
		volume2.adicionarItem(new NoDocumento("Documento 3"));
		volume2.adicionarItem(documento4);

		this.noProcesso = new NoProcesso("Processo 1");
		noProcesso.adicionarItem(volume1);
		noProcesso.adicionarItem(volume2);
	}

	public void selecaoModificada(TreeSelectionChangeEvent selectionChangeEvent) {
		// considering only single selection
		List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
		Object currentSelectionKey = selection.get(0);
		UITree tree = (UITree) selectionChangeEvent.getSource();

		Object storedKey = tree.getRowKey();
		tree.setRowKey(currentSelectionKey);
		currentSelection = (TreeNode) tree.getRowData();
		tree.setRowKey(storedKey);
	}
}