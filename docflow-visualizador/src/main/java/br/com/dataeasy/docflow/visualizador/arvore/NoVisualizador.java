package br.com.dataeasy.docflow.visualizador.arvore;

import java.io.Serializable;

import javax.swing.tree.TreeNode;

/**
 * <b>Title:</b> NoComNome.java <br>
 * <b>Description:</b>Nó pai dos nós utilizados na árvore de itens do visualizador de documentos.<br>
 * <b>Package:</b> br.com.dataeasy.docflow.visualizador.arvore <br>
 * <b>Project:</b> docflow-visualizador <br>
 * <b>Company:</b> DataEasy Consultoria e Informática LTDA. <br>
 * 
 * Copyright (c) 2014 DataEasy - Todos os direitos reservados.
 * 
 * @author rafael.fontoura
 * @version Revision: $ Date: 11/03/2014
 */
public abstract class NoVisualizador implements Serializable, TreeNode {

	protected static final String PROCESSO = "PROCESSO";
	protected static final String DOCUMENTO = "DOCUMENTO";
	protected static final String ANEXO = "ANEXO";
	protected static final String VOLUME = "VOLUME";
	private static final long serialVersionUID = 3954490553858795226L;

	private String nome;
	private Long id;
	
	public NoVisualizador() {
		super();
	}

	public NoVisualizador(String nome) {
		super();
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public abstract String getTipo();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public abstract void setNoPai(NoVisualizador noVisualizador);
}
