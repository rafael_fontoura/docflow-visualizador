package br.com.dataeasy.docflow.visualizador.listener;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import br.com.dataeasy.docflow.visualizador.service.VisualizadorDocumentoService;

import com.atalasoft.licensing.LicenseValidator;

public class LicencaJoltImageContextListener implements ServletContextListener {

	private static final Logger LOG = Logger.getLogger(LicencaJoltImageContextListener.class);

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
	}

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		limparDocumentosTemporarios(sce);
		// MACAddressUtil.getMacAddress();
		validarLicencaJoltImagePorArquivoLicenca(sce);
		// validarLicencaJoltImagePorString();
	}

	private void limparDocumentosTemporarios(ServletContextEvent sce) {
		File diretorioTemp = new File(sce.getServletContext().getRealPath(
				VisualizadorDocumentoService.DIRETORIO_SESSAO_USUARIOS));
		if (diretorioTemp.exists() && diretorioTemp.isDirectory()) {
			try {
				FileUtils.deleteDirectory(diretorioTemp);
			} catch (IOException e) {
				LOG.error("Erro excluindo diretório " + diretorioTemp + ".", e);
			}
		}
		diretorioTemp.mkdir();
	}

	private void validarLicencaJoltImagePorArquivoLicenca(ServletContextEvent sce) {
		ServletContext servletContext = sce.getServletContext();
		String nomeArquivo = servletContext.getInitParameter("arquivoLicencaJoltImage");
		String caminhoArquivo = servletContext.getRealPath("WEB-INF" + File.separator + "classes" + File.separator
				+ nomeArquivo);
		File arquivo = new File(caminhoArquivo);
		LicenseValidator.validateLicense(arquivo);
	}

	@SuppressWarnings("unused")
	private void validarLicencaJoltImagePorString() {
		String licencaStr = "<?xml version='1.0' encoding='utf-8'?><License>  "
				+ "<Assembly>imaging.ImagingProduct</Assembly>  <Version>10.4</Version>  "
				+ "<SerialNumber>JAI2-49C3-9F4E-3B0B-2CE5</SerialNumber>  <Name>Rafael Fontoura | DataEasy</Name>  "
				+ "<MachineID>00:1F:3C:0A:E4:D1</MachineID>  <Flags />  <MachineName>RAFAEL-NB</MachineName>  "
				+ "<UserName>Rafael</UserName>  <Flag name='v2Signature' />  "
				+ "<Signature>omIUvhzgaTPG2Lc7/xnXS44yCs/uxBEiYCNai7E524HUMgTFwiK3jagQsMgyyINiHhafhz0SmuehGq7WrBpXF+i3G8"
				+ "7jt9fSFkjLC8KWhppl2UxVixhJlejhjut9xaWkMg+/LD1krDGPutw7bk2sHqX0nCMjSF8sFRlNHTCbvds=</Signature></License>";
		LicenseValidator.validateLicense(licencaStr);
	}
}
