package br.com.dataeasy;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

import org.apache.log4j.Logger;

/**
 * <b>Title:</b> MACAddressUtil.java <br>
 * <b>Description:</b> <br>
 * <b>Package:</b> br.com.dataeasy <br>
 * <b>Project:</b> docflow-visualizador <br>
 * <b>Company:</b> DataEasy Consultoria e Informática LTDA. <br>
 * 
 * Copyright (c) 2014 DataEasy - Todos os direitos reservados.
 * 
 * @author Rafael
 * @version Revision: $ Date: 19/03/2014
 */
public class MACAddressUtil {

	private static final Logger LOG = Logger.getLogger(MACAddressUtil.class);

	public static InetAddress getMacAddress() {
		InetAddress ip;
		try {

			ip = InetAddress.getLocalHost();
			System.out.println("Current IP address : " + ip.getHostAddress());

			NetworkInterface network = NetworkInterface.getByInetAddress(ip);

			byte[] mac = network.getHardwareAddress();

			System.out.print("Current MAC address : ");

			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < mac.length; i++) {
				sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
			}
			LOG.info("MAC address: " + sb.toString());
			return ip;
		} catch (UnknownHostException e) {
			throw new RuntimeException("Erro recuperando MAC address", e);
		} catch (SocketException e) {
			throw new RuntimeException("Erro recuperando MAC address", e);
		}
	}
}