var _viewer;
var _serverUrl = "visualizar.view";
var _docUrl = "";
var _testing;
var _larguraInicialVisualizador;
var _larguraVisualizadorExcetoThumbs;
var _mostrandoThumbs = true;
var habilitarThumbs = false;
var _habilitarAnotacoes = false;

var nomeArquivo = null;
var caminhoArquivoOriginalDownload = null;
var mimeType = null;

$(function() {
	inicializarVisualizador();

	window.onresize = function() {
		var altura = $(".inner-viewer").height();
		if (!altura || altura == 0) {
			altura = $(window).height() - 150;
			if (altura < 300) {
				altura = 300;
			}
		}

		$(".atala-document-viewer").height(altura);
		if (habilitarThumbs) {
			$(".atala-document-thumbs").height(altura);
			$(".clickmeleft").height(altura);
			$(".clickmeright").height(altura);
		}
	};

	adicionarBotoesBarraFerramentas();
	if (habilitarThumbs) {
		configurarBarraClick();
	}
	traduzirLabelsBotoes();
});

function inicializarVisualizador() {
	try {
		_viewer = new Atalasoft.Controls.WebDocumentViewer({
			parent : $(".atala-document-viewer"),
			toolbarparent : $(".atala-document-toolbar"),
			serverurl : _serverUrl,
			documenturl : _docUrl,
			allowannotations : _habilitarAnotacoes,
			showerrors : false,
			showstatus : false,
			showbuttontext : false
		});

		if (habilitarThumbs) {
			_thumbs = new Atalasoft.Controls.WebDocumentThumbnailer({
				parent : $(".atala-document-thumbs"),
				serverurl : _serverUrl,
				documenturl : _docUrl,
				allowannotations : _habilitarAnotacoes,
				viewer : _viewer
			});
		} else {
			$(".atala-document-thumbs").hide();
			$(".clickmeleft").hide();
		}

		_viewer.bind({
			"error" : onError,
			// "documentsaved" : onDocumentSaved,
			"documentloaded" : onDocumentLoaded
		});
	} catch (e) {
		throw e;
	}

	function onError(e) {
		_testing = e.message;
		throw e;
	}

	function onDocumentSaved(e) {

	}

	function onDocumentLoaded(e) {
		var a = "blah";
	}

	if (_habilitarAnotacoes) {
		_viewer.setAnnotationDefaults([ {
			type : "line",
			outline : {
				color : "#f00",
				opacity : 0.80,
				width : 5
			}
		}, {
			type : "freehand",
			outline : {
				color : "#00f",
				opacity : 0.80,
				width : 15
			}
		}, {
			type : "text",
			text : {
				value : "Double-click to change text",
				align : "left",
				font : {
					color : "#009",
					family : "Times New Roman",
					size : 36
				}
			},
			outline : {
				color : "#00a",
				opacity : 0.80,
				width : 1
			},
			fill : {
				color : "#ff9",
				opacity : 0.50
			}
		}, {
			type : "rectangle",
			fill : {
				color : "black",
				opacity : 1
			},
		} ]);
	}

	// Não mostrar botão de Salvar. Será chamado programaticamente
	$(".atala-ui-icon-save").parent().css("display", "none");

	definirLarguraVisualizador();
}

function adicionarBotoesBarraFerramentas() {
	if (!$(".PrintToolbar").is("*")) {
		var toolbar = $('<div />');
		toolbar.addClass('PrintToolbar');
		toolbar.append(adicionarBotaoImprimir());
		
		$('.atala-document-toolbar').prepend(toolbar);
	}
	
	if (!$(".SaveToolbar").is("*")) {
		var toolbar = $('<div />');
		toolbar.addClass('SaveToolbar');
		toolbar.append(adicionarBotaoSalvar());

		$('.atala-document-toolbar').prepend(toolbar);
	}
}

function adicionarBotaoSalvar() {
	var saveButton = $('<button id="undefined_wdv1_toolbar_Button_SalvarArquivo" title="Salvar Arquivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only atala-ui-button" role="button">Salvar Arquivo</button>');
	saveButton.click(function() {
		salvarArquivo();
	});
	saveButton.button({
		icons : {
			primary : 'atala-ui-icon atala-ui-icon-save'
		},
		text : false
	});

	return saveButton;
}

function adicionarBotaoImprimir() {
	var printButton = $('<button id="undefined_wdv1_toolbar_Button_ImprimirArquivo" title="Imprimir" class="ui-button ui-widget ui-corner-all ui-button-icon-only atala-ui-button" role="button">Imprimir</button>');
	printButton.click(function(e) {
		gravarArquivoParaImpressao(nomeArquivo, caminhoArquivoOriginalDownload);
	});
	printButton.button({
		icons : {
			primary : 'atala-ui-icon-extra atala-ui-icon-print'
		},
		text : false
	});
	
	return printButton;
}

function configurarBarraClick() {

	var barraClick = $(".clickmeleft");

	barraClick.click(function(event) {
		var mainThumbs = $(".atala-document-thumbs");

		if (_mostrandoThumbs) {
			mainThumbs.animate({
				width : "0px"
			}, 500, function() {
				mainThumbs.hide();
				$(".atala-document-viewer").animate({
					width : _larguraVisualizadorExcetoThumbs
				}, 250, function() {
				});
			});

			_mostrandoThumbs = false;
			barraClick.removeClass("clickmeleft");
			barraClick.removeClass("clickmehoverleft");
			barraClick.addClass("clickmeright");
		} else {
			mainThumbs.show();
			mainThumbs.animate({
				width : "130px"
			}, 500);

			_mostrandoThumbs = true;
			$(".atala-document-viewer").css("width",
					_larguraInicialVisualizador);
			barraClick.removeClass("clickmeright");
			barraClick.removeClass("clickmehoverright");
			barraClick.addClass("clickmeleft");
		}
	});

	barraClick.hover(function() {
		if (_mostrandoThumbs)
			barraClick.addClass("clickmehoverleft");
		else
			barraClick.addClass("clickmehoverright");
	}, function() {
		if (_mostrandoThumbs)
			barraClick.removeClass("clickmehoverleft");
		else
			barraClick.removeClass("clickmehoverright");
	});

}

function definirLarguraVisualizador() {
	var larguraTotal = parseInt($(".main-viewer").css("width"), 10);
	var larguraThumbs = (habilitarThumbs ? parseInt($(".atala-document-thumbs")
			.css("width"), 10) : 0);
	var larguraClick = parseInt($(".clickmeleft").css("width"), 10);

	_larguraInicialVisualizador = larguraTotal;
	if (habilitarThumbs) {
		_larguraInicialVisualizador = _larguraInicialVisualizador
				- (larguraThumbs + larguraClick + 3);
	}
	_larguraVisualizadorExcetoThumbs = larguraTotal - larguraClick - 2;
	$(".atala-document-viewer").css("width", _larguraInicialVisualizador);
}

function traduzirLabelsBotoes() {
	atualizarTitulo("_Button_PagePrev", "P\u00E1gina Anterior");
	atualizarTitulo("_Button_PageNext", "Pr\u00F3xima P\u00E1gina");
	atualizarTitulo("_Butto" + "n_ZoomOut", "Diminuir Zoom");
	atualizarTitulo("_Button_ZoomIn", "Aumentar Zoom");
	atualizarTitulo("_Button_FitNone", "Tamanho Real");
	atualizarTitulo("_Button_FitBest", "Melhor Ajuste");
	atualizarTitulo("_Button_FitWidth", "Ajustar \u00E0 Largura");
	atualizarTitulo("_Button_Ellipse", "Desenhar Elipse");
	atualizarTitulo("_Button_Highlight", "Desenhar Realce");
	atualizarTitulo("_Button_Line", "Desenhar Linha");
	atualizarTitulo("_Button_Lines", "Desenhar Polilinha");
	atualizarTitulo("_Button_Freehand", "Desenho Livre");
	atualizarTitulo("_Button_Rectangle", "Desenhar Ret\u00E2ngulo");
	atualizarTitulo("_Button_Text", "Inserir Texto");
}

function atualizarTitulo(idBotaoTerminaCom, titulo) {
	$("button[id$='" + idBotaoTerminaCom + "']").attr("title", titulo);
}

function isArquivoVisualizavel(nomeArquivo) {
	if (nomeArquivo) {
		var extensao = nomeArquivo.substring(nomeArquivo.lastIndexOf(".") + 1).toLowerCase();
		return ["png", "bmp", "pdf", "tif", "tiff", "jpg", "jpeg"].indexOf(extensao) > -1;
	}
	return false;
}

var Visualizador = {
	abrir : function(caminhoArquivo) {
		caminhoArquivoDownload = caminhoArquivo;
		caminhoArquivoOriginalDownload = caminhoArquivo;

		if (isArquivoVisualizavel(caminhoArquivo)) {
			if (caminhoArquivoDownload != null) {
				_viewer.OpenUrl(caminhoArquivoDownload);
			}
		} else {
			//Visualizador.salvarArquivo();
		}
	},

	salvarArquivo : function() {
		// tratando modificação do ID dos inputs devido à concatenação de IDs de componentes-pai do JSF
		// desta forma, pegando componente "que inicia com :_nomeArquivoDownload" 
		
		var botao1 = $("input[type='submit'][id$='\:_downloadArquivo'");
		if (botao1.is("*")) {
			$("input[type='hidden'][id$='\:_nomeArquivoDownload']").val(nomeArquivo);
			$("input[type='hidden'][id$='\:_caminhoArquivoDownload'").val(caminhoArquivoOriginalDownload);
			botao1.click();
		} else {
			$("#_nomeArquivoDownload").val(nomeArquivo);
			$("#_caminhoArquivoDownload").val(caminhoArquivoOriginalDownload);
			$("#_mimeType").val(mimeType);
			$("#_downloadArquivo").click();
		}
	},
	
	imprimirArquivo : function(caminhoArquivoImpressao) {
		var w = window.open(caminhoArquivoImpressao);
		setTimeout(function() {
			w.print();
			w.close();
		}, 1000);
	},
	
	irParaPagina : function(numeroPagina) {
		_viewer.showPage(numeroPagina - 1);
	}
};