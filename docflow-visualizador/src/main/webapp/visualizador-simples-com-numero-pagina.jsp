<%@ page
	language="java"
	contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta
	http-equiv="Content-Type"
	content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<script
	src="resources/WebDocViewer/jquery-1.7.1.min.js"
	type="text/javascript"></script>
<script
	src="resources/WebDocViewer/atalaWebDocumentViewer.js"
	type="text/javascript"></script>
<link
	href="resources/WebDocViewer/atalaWebDocumentViewer.css"
	rel="Stylesheet"
	type="text/css" />
</head>
<body>
	<table>
		<tr>
			<td>
				<div
					id="_toolbar1"
					class="atala-document-toolbar"
					style="width: 670px;"></div>
				<div
					id="_container1"
					class="atala-document-container"
					style="width: 670px; height: 500px;"></div>
			</td>
		</tr>
	</table>
	<script
		type="text/javascript"
		language="javascript">
		var _docUrl = 'exemplo/6page.pdf';
		var _serverUrl = 'visualizar.view';
		var _savepath = "save/save.xmp";
		var _viewer = null;

		try {
			_viewer = new Atalasoft.Controls.WebDocumentViewer({
				parent : $('#_container1'), // parent container to put the viewer in
				toolbarparent : $('#_toolbar1'), // parent container to put the viewer toolbar in
				serverurl : _serverUrl, // server handler url to send image requests to
				documenturl : _docUrl, // document url relative to the server handler url
				allowannotations : true,
				allowflick : true,
				savepath : _savepath,
				showpagenumber : true,
				showbuttontext : false
			});
		} catch (e) {
			alert("Erro inicializando visualizador: " + e.message);
		}

		function criarNumeroDePagina(paginaInicial, paginaFinal, iniciarCom) {
			var coord = 50;
			var _w = 35;
			var _h = 35;
			
			
			var nrPagina = iniciarCom;
			for (var i = paginaInicial; i <= paginaFinal; i++) {
				var anotacaoTexto = {
					type : 'text',
					readonly : true,
					movable : false,
					selectable : false,
					resizable : false,
					x : coord,
					y : coord,
					width : _w,
					height : _h,
					outline : {
						color : 'black',
						width : 0
					},
					fill : {
						opacity : 0
					},
					text : {
						value : nrPagina + '',
						align : 'center',
						font : {
							bold : true,
							color : 'black',
							family : 'Arial Black',
							size : 16
						}
					}
				};

				var anotacaoCirculo = {
					type : 'ellipse',
					readonly : true,
					movable : false,
					selectable : false,
					resizable : false,
					x : coord - 3,
					y : coord - 2,
					height : _h,
					width : _w,
					outline : {
						color : 'black',
						width : 1
					},
					fill : {
						color : '#FFFF88',
						opacity : 1
					}
				};

				var circulo = _viewer.annotations.createOnPage(anotacaoCirculo, i);
				var texto = _viewer.annotations.createOnPage(anotacaoTexto, i);
				nrPagina++;
			}
		}

		_viewer.bind('documentloaded', function() {
			criarNumeroDePagina(0, 5, 18);
		});
	</script>
</body>
</html>