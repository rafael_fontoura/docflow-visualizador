package br.com.dataeasy.docflow.visualizador.conversao;

public class FalhaNaConversaoException extends RuntimeException {

	private static final long serialVersionUID = -3179637452295440659L;

	public FalhaNaConversaoException(String mensagem, Throwable e) {
		super(mensagem, e);
	}

	public FalhaNaConversaoException(String mensagem) {
		super(mensagem);
	}

	public FalhaNaConversaoException() {
		super();
	}

	public FalhaNaConversaoException(Throwable e) {
		super(e);
	}

}
