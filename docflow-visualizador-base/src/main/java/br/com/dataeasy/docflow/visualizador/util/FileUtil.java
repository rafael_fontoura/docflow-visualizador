package br.com.dataeasy.docflow.visualizador.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;

public class FileUtil {

	public static void escreverArquivo(File file, byte[] conteudo) throws IOException {
		FileUtils.writeByteArrayToFile(file, conteudo);
	}

	public static byte[] getByteArray(InputStream is) throws IOException {
		byte[] buffer = new byte[8192];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		int bytesRead;
		while ((bytesRead = is.read(buffer)) != -1) {
			baos.write(buffer, 0, bytesRead);
		}
		return baos.toByteArray();
	}

	public static String calcularMD5Checksum(InputStream is) throws IOException {
		return DigestUtils.md5Hex(is);
	}
}