package br.com.dataeasy.docflow.visualizador.conversao;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

public class DocX4JConversorDocxParaPDF {

	public static void converterParaPDF(InputStream is, File filePDFResultado) throws IOException {
		// 1) Load DOCX into XWPFDocument
		//InputStream is = new FileInputStream(fileDocX);
		XWPFDocument document = new XWPFDocument(is);

		// 2) Prepare Pdf options
		PdfOptions options = PdfOptions.create();

		// 3) Convert XWPFDocument to Pdf
		OutputStream out = new FileOutputStream(filePDFResultado);
		PdfConverter.getInstance().convert(document, out, options);
	}
}