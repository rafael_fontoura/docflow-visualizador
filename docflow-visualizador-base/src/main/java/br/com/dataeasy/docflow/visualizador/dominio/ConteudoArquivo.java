package br.com.dataeasy.docflow.visualizador.dominio;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_CONTEUDO_ARQUIVO")
@SequenceGenerator(name = "seqConteudoArquivoGenerator", sequenceName = "SQ_CONTEUDO_ARQUIVO")
public class ConteudoArquivo implements Serializable {

	private static final long serialVersionUID = -657429116095387312L;

	@Id
	@Column(name = "ID_CONTEUDO_ARQUIVO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqConteudoArquivoGenerator")
	private Integer id;

	@OneToOne(mappedBy = "conteudoArquivo")
	private Arquivo arquivo;

	@Column(name = "CONTEUDO", nullable = false)
	private byte[] conteudo;

	public ConteudoArquivo() {
		super();
	}

	public byte[] getConteudo() {
		return this.conteudo;
	}

	public void setConteudo(byte[] conteudo) {
		this.conteudo = conteudo;
	}

	public Arquivo getArquivo() {
		return arquivo;
	}

	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
}