package br.com.dataeasy.docflow.visualizador.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;

@Repository
public class ArquivoDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<Arquivo> listar() {
		return em.createQuery("from br.com.dataeasy.docflow.visualizador.dominio.Arquivo").getResultList();
	}
	
	public Arquivo obter(Integer idArquivo) {
		return em.find(Arquivo.class, idArquivo);
	}

}
