package br.com.dataeasy.docflow.visualizador.conversao;

public class ConversaoNaoSuportadaExeption extends RuntimeException {

	private static final long serialVersionUID = -7814873552058557323L;

	public ConversaoNaoSuportadaExeption(String mensagem, Throwable e) {
		super(mensagem, e);
	}

	public ConversaoNaoSuportadaExeption(String mensagem) {
		super(mensagem);
	}

	public ConversaoNaoSuportadaExeption() {
		super();
	}

}
