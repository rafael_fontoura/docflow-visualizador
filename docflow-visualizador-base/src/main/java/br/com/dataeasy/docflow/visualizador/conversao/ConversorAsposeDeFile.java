package br.com.dataeasy.docflow.visualizador.conversao;

import java.io.File;
import java.util.Arrays;

import org.springframework.stereotype.Service;

import com.aspose.cells.Workbook;
import com.aspose.slides.Presentation;
import com.aspose.slides.PresentationEx;
import com.aspose.words.Document;

@Service
public class ConversorAsposeDeFile extends ConversorAspose {

	public File converterParaPDF(File origem) {
		return converterParaPDF(new Object[] {origem});
	}
	
	protected File converterParaPDF(Object... entradas) {
		File origem = (File) entradas[0];

		String formato = ArquivoUtil.getExtensao(origem.getName());
		File resultado = null;
		try {
			if (Arrays.asList("doc", "docx", "odt").contains(formato)) {
				resultado = converterWordParaPDF(origem);
			} else if (Arrays.asList("xls", "xlsx", "ods").contains(formato)) {
				resultado = converterExcelParaPDF(origem);
			} else if (Arrays.asList("pptx").contains(formato)) {
				resultado = converterPPTXParaPDF(origem);
			} else if (Arrays.asList("ppt").contains(formato)) {
				resultado = converterPPTParaPDF(origem);
			} else if ("odp".equals(formato)) {
				try {
					resultado = converterPPTParaPDF(origem);
				} catch (Exception e) {
					resultado = converterPPTXParaPDF(origem);
				}
			}
			return resultado;
		} catch (Exception e) {
			throw new FalhaNaConversaoException("Erro convertedo arquivo " + origem.getName() + " para PDF.", e);
		}
	}

	private File converterPPTXParaPDF(File origem) throws Exception {
		PresentationEx pres = new PresentationEx(origem.getCanonicalPath());
		String nomePDF = criarNomeDocumentoPDF(origem);
		return super.converterPPTXParaPDF(pres, nomePDF);
	}

	private File converterPPTParaPDF(File origem) throws Exception {
		Presentation pres = new Presentation(origem.getCanonicalPath());
		String nomePDF = criarNomeDocumentoPDF(origem);
		return super.converterPPTParaPDF(pres, nomePDF);
	}

	private File converterExcelParaPDF(File origem) throws Exception {
		Workbook workbook = new Workbook(origem.getCanonicalPath());
		String nomePDF = criarNomeDocumentoPDF(origem);
		return super.converterExcelParaPDF(workbook, nomePDF);
	}

	private File converterWordParaPDF(File origem) throws Exception {
		Document doc = new Document(origem.getCanonicalPath());
		String nomePDF = criarNomeDocumentoPDF(origem);
		return super.converterWordParaPDF(doc, nomePDF);
	}
}