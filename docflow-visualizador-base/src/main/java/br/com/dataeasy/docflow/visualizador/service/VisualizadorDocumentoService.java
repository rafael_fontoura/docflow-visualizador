package br.com.dataeasy.docflow.visualizador.service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;
import br.com.dataeasy.docflow.visualizador.util.FileUtil;
import br.com.dataeasy.docflow.visualizador.util.UtilString;

@Service
public class VisualizadorDocumentoService {

	public static final String DIRETORIO_SESSAO_USUARIOS = "/temp";
	private static final Logger LOG = Logger.getLogger(VisualizadorDocumentoService.class);

	public File gravarDocumentoEmDisco(Arquivo arquivoNoBanco) throws IOException {
		File diretorio = recuperarDiretorioDaSessao();
		diretorio.mkdir();

		String nomeDocFormatado = formatarNomeDocumento(arquivoNoBanco.getNome());
		File arquivoEmDisco = new File(diretorio, nomeDocFormatado);
		if (!arquivoEmDisco.exists()) {
			LOG.info("Arquivo " + nomeDocFormatado + " não existe. Gravando em disco...");
			byte[] conteudo = arquivoNoBanco.getConteudoArquivo().getConteudo();
			FileUtil.escreverArquivo(arquivoEmDisco, conteudo);
		} else if (!isMesmoArquivo(arquivoNoBanco, arquivoEmDisco)) {
			LOG.info("Arquivo " + nomeDocFormatado
					+ " existe, mas não é o mesmo. Apagando e gravando em disco...");
			arquivoEmDisco.delete();
			byte[] conteudo = arquivoNoBanco.getConteudoArquivo().getConteudo();
			FileUtil.escreverArquivo(arquivoEmDisco, conteudo);
		}
		return arquivoEmDisco;
	}

	public boolean isMesmoArquivo(Arquivo arquivoEmBanco, File arquivoEmDisco) throws IOException,
			FileNotFoundException {
		// calculando MD5 checksum para ver se é o mesmo arquivo
		byte[] conteudo = arquivoEmBanco.getConteudoArquivo().getConteudo();
		ByteArrayInputStream bais = new ByteArrayInputStream(conteudo);
		String checksumArquivoBanco = FileUtil.calcularMD5Checksum(bais);

		FileInputStream fis = new FileInputStream(arquivoEmDisco);
		String checksumArquivoDisco = FileUtil.calcularMD5Checksum(fis);
		return checksumArquivoBanco.equals(checksumArquivoDisco);
	}

	public File recuperarDiretorioDaSessao(HttpSession sessao) {
		String root = sessao.getServletContext().getRealPath("/");
		return new File(root + DIRETORIO_SESSAO_USUARIOS.substring(1) + File.separator + sessao.getId());
	}

	public File recuperarDiretorioDaSessao() {
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
		HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
		return recuperarDiretorioDaSessao(request.getSession());
	}

	public boolean documentoExisteEmDisco(File documento) {
		String nomeDocumento = formatarNomeDocumento(documento.getName());
		File diretorio = documento.getParentFile();
		File documentoEmDisco = (diretorio == null ? new File(nomeDocumento) : new File(diretorio, nomeDocumento));
		return documentoEmDisco.exists();
	}

	public String formatarNomeDocumento(String nomeDocumento) {
		nomeDocumento = nomeDocumento.replaceAll("[ ]", "%20");
		return UtilString.removerAcentuacao(nomeDocumento);
	}
}