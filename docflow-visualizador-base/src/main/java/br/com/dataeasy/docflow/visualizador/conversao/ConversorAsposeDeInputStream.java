package br.com.dataeasy.docflow.visualizador.conversao;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Arrays;

import org.springframework.stereotype.Service;

import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;

import com.aspose.cells.Workbook;
import com.aspose.slides.Presentation;
import com.aspose.slides.PresentationEx;
import com.aspose.words.Document;

@Service
public class ConversorAsposeDeInputStream extends ConversorAspose {

	protected File converterPPTXParaPDF(Arquivo arquivoOrigem, File diretorioDestino) throws Exception {
		ByteArrayInputStream is = new ByteArrayInputStream(arquivoOrigem.getConteudoArquivo().getConteudo());
		PresentationEx pres = new PresentationEx(is);
		String nomePDF = criarNomeDocumentoPDF(diretorioDestino, arquivoOrigem);

		return super.converterPPTXParaPDF(pres, nomePDF);
	}

	protected File converterPPTParaPDF(Arquivo arquivoOrigem, File diretorioDestino) throws Exception {
		ByteArrayInputStream is = new ByteArrayInputStream(arquivoOrigem.getConteudoArquivo().getConteudo());
		Presentation pres = new Presentation(is);
		String nomePDF = criarNomeDocumentoPDF(diretorioDestino, arquivoOrigem);
		return super.converterPPTParaPDF(pres, nomePDF);
	}

	protected File converterExcelParaPDF(Arquivo arquivoOrigem, File diretorioDestino) throws Exception {
		ByteArrayInputStream is = new ByteArrayInputStream(arquivoOrigem.getConteudoArquivo().getConteudo());
		Workbook workbook = new Workbook(is);
		String nomePDF = criarNomeDocumentoPDF(diretorioDestino, arquivoOrigem);
		return super.converterExcelParaPDF(workbook, nomePDF);
	}

	protected File converterWordParaPDF(Arquivo arquivoOrigem, File diretorioDestino) throws Exception {
		ByteArrayInputStream is = new ByteArrayInputStream(arquivoOrigem.getConteudoArquivo().getConteudo());
		Document doc = new Document(is);
		String nomePDF = criarNomeDocumentoPDF(diretorioDestino, arquivoOrigem);
		return super.converterWordParaPDF(doc, nomePDF);
	}

	public File converterParaPDF(Arquivo arquivoOrigem, File diretorioDestino) {
		return converterParaPDF(new Object[] { arquivoOrigem, diretorioDestino });
	}

	@Override
	protected File converterParaPDF(Object... entradas) {
		Arquivo arquivo = (Arquivo) entradas[0];
		File diretorioDestino = (File) entradas[1];
		String formato = ArquivoUtil.getExtensao(arquivo.getNome());

		File resultado = null;

		try {
			if (Arrays.asList("doc", "docx", "odt").contains(formato)) {
				resultado = converterWordParaPDF(arquivo, diretorioDestino);
			} else if (Arrays.asList("xls", "xlsx", "ods").contains(formato)) {
				resultado = converterExcelParaPDF(arquivo, diretorioDestino);
			} else if (Arrays.asList("pptx").contains(formato)) {
				resultado = converterPPTXParaPDF(arquivo, diretorioDestino);
			} else if (Arrays.asList("ppt").contains(formato)) {
				resultado = converterPPTParaPDF(arquivo, diretorioDestino);
			} else if ("odp".equals(formato)) {
				try {
					resultado = converterPPTParaPDF(arquivo, diretorioDestino);
				} catch (Exception e) {
					resultado = converterPPTXParaPDF(arquivo, diretorioDestino);
				}
			}
			return resultado;
		} catch (Exception e) {
			throw new FalhaNaConversaoException("Erro convertedo arquivo " + arquivo.getNome() + " para PDF.", e);
		}
	}
}
