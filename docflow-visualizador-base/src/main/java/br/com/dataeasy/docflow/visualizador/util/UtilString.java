package br.com.dataeasy.docflow.visualizador.util;

public final class UtilString {

	/**
	 * Remove a acentuação de uma string.
	 * 
	 * @param string
	 *            string
	 * @return string sem acentos.
	 */
	public static String removerAcentuacao(String string) {
		if (string != null) {
			string = string.replaceAll("[ÁÀÂÃ]", "A");
			string = string.replaceAll("[áàâãª]", "a");
			string = string.replaceAll("[ÊÈÉË]", "E");
			string = string.replaceAll("[êèéë]", "e");
			string = string.replaceAll("[ÎÍÌÏ]", "I");
			string = string.replaceAll("[îíìï]", "i");
			string = string.replaceAll("[ÔÕÒÓÖ]", "O");
			string = string.replaceAll("[óòôõºö]", "o");
			string = string.replaceAll("[ÛÙÚÜ]", "U");
			string = string.replaceAll("[ûúùü]", "u");
			string = string.replaceAll("[Ç]", "C");
			string = string.replaceAll("[ç]", "c");
			string = string.replaceAll("[ñ]", "n");
			string = string.replaceAll("[Ñ]", "N");
		}
		return string;
	}
}