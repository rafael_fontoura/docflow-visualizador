package br.com.dataeasy.docflow.visualizador.service;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.dataeasy.docflow.visualizador.conversao.ArquivoUtil;
import br.com.dataeasy.docflow.visualizador.conversao.ConversorAsposeDeInputStream;
import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;
import br.com.dataeasy.docflow.visualizador.repository.ArquivoDAO;

@Service
@Transactional(readOnly = true)
public class ArquivoService implements Serializable {

	private static final long serialVersionUID = -8939245096652795233L;

	// private static final Logger LOG = Logger.getLogger(ArquivoService.class);
	private static final List<String> FORMATOS_VISUALIZACAO_SUPORTADOS = Arrays.asList("bmp", "jpg", "jpeg", "pdf",
			"png", "tif", "tiff", "txt", "xml");

	@Resource
	private ConversorAsposeDeInputStream conversorAsposeDeInputStream;

	@Resource
	private ArquivoDAO arquivoDAO;

	@Resource
	private VisualizadorDocumentoService visualizadorDocumentoService;

	public List<Arquivo> listar() {
		return arquivoDAO.listar();
	}

	private File gravarDocumentoEmDisco(Arquivo arquivo) {
		try {
			return visualizadorDocumentoService.gravarDocumentoEmDisco(arquivo);
		} catch (IOException e) {
			String summary = "Erro escrevendo arquivo " + arquivo.getNome();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, summary);
			FacesContext.getCurrentInstance().addMessage(null, message);
			return null;
		}
	}

	private File converterParaPDF(Arquivo arquivo) {
		File diretorio = visualizadorDocumentoService.recuperarDiretorioDaSessao();
		return conversorAsposeDeInputStream.converterParaPDF(arquivo, diretorio);
	}

	public String recuperarArquivoParaVisualizacao(Integer idArquivo) {
		Arquivo arquivo = arquivoDAO.obter(idArquivo);
		arquivo.getConteudoArquivo().getConteudo();
		String nomeArquivoRetorno = null;
		if (isDocumentoVisualizavel(arquivo.getNome())) {
			File retorno = gravarDocumentoEmDisco(arquivo);
			nomeArquivoRetorno = retorno.getName();
		} else if (isPossivelConverter(arquivo.getNome())) {
			if (isNecessariaConversaoEGravacao(arquivo)) {
				
				File pdf = converterParaPDF(arquivo);
				nomeArquivoRetorno = pdf.getName();
			} else {
				File diretorioDestino = visualizadorDocumentoService.recuperarDiretorioDaSessao();
				try {
					String nomeCompletoRetorno = conversorAsposeDeInputStream.criarNomeDocumentoPDF(diretorioDestino, arquivo);
					nomeArquivoRetorno = new File(nomeCompletoRetorno).getName();
				} catch (IOException e) {
					String summary = "Erro recuperando diretório da sessão do usuário.";
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, summary);
					FacesContext.getCurrentInstance().addMessage(null, message);
					return null;
				}
			}
		}
		return nomeArquivoRetorno;
	}

	private boolean isNecessariaConversaoEGravacao(Arquivo arquivo) {
		boolean necessariaConversaoGravacao = false;
		File diretorio = visualizadorDocumentoService.recuperarDiretorioDaSessao();
		try {
			necessariaConversaoGravacao = !conversorAsposeDeInputStream.documentoPDFExiste(arquivo, diretorio);
		} catch (IOException e) {
			String summary = "Erro verificando existência de arquivo PDF no servidor.";
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, summary, summary);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
		return necessariaConversaoGravacao;
	}

	private boolean isPossivelConverter(String nome) {
		String extensao = ArquivoUtil.getExtensao(nome);
		return conversorAsposeDeInputStream.permiteConversao(extensao);
	}

	private boolean isDocumentoVisualizavel(String nome) {
		String extensao = ArquivoUtil.getExtensao(nome);
		return FORMATOS_VISUALIZACAO_SUPORTADOS.contains(extensao);
	}
}
