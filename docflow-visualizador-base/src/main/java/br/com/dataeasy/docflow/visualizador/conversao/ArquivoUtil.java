package br.com.dataeasy.docflow.visualizador.conversao;

public class ArquivoUtil {

	public static String getExtensao(String nomeArquivo) {
		return nomeArquivo.substring(nomeArquivo.lastIndexOf('.') + 1).toLowerCase();
	}
}
