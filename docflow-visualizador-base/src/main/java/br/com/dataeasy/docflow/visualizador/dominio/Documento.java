package br.com.dataeasy.docflow.visualizador.dominio;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "TB_DOCUMENTO")
@SequenceGenerator(name = "seqDocumentoGenerator", sequenceName = "SQ_DOCUMENTO")
public class Documento implements Serializable {

	private static final long serialVersionUID = 8543511502957857345L;

	@Id
	@Column(name = "ID_DOCUMENTO")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqDocumentoGenerator")
	private Integer id;
	
	@Column(name = "NM_DOCUMENTO", length = 100, nullable = false)
	private String nome;
	
	@Column(name = "DT_CRIACAO", nullable = false)
	private Date dataCriacao;
	
	@ManyToOne
	@JoinColumn(name = "ID_PROCESSO")
	private Processo processo;
	
	@OneToOne(cascade = {CascadeType.REMOVE, CascadeType.PERSIST})
	@JoinColumn(name = "ID_ARQUIVO", referencedColumnName = "ID_ARQUIVO")
	private Arquivo arquivo;
	
	@OneToMany(mappedBy = "documento")
	private List<Anexo> anexos;
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the dataCriacao
	 */
	public Date getDataCriacao() {
		return dataCriacao;
	}

	/**
	 * @param dataCriacao
	 *            the dataCriacao to set
	 */
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	/**
	 * @return the arquivo
	 */
	public Arquivo getArquivo() {
		return arquivo;
	}

	/**
	 * @param arquivo
	 *            the arquivo to set
	 */
	public void setArquivo(Arquivo arquivo) {
		this.arquivo = arquivo;
	}

	/**
	 * @return the anexos
	 */
	public List<Anexo> getAnexos() {
		return anexos;
	}

	/**
	 * @param anexos the anexos to set
	 */
	public void setAnexos(List<Anexo> anexos) {
		this.anexos = anexos;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the processo
	 */
	public Processo getProcesso() {
		return processo;
	}

	/**
	 * @param processo the processo to set
	 */
	public void setProcesso(Processo processo) {
		this.processo = processo;
	}
}
