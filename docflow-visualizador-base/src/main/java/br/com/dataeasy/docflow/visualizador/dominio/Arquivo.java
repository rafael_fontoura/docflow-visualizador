package br.com.dataeasy.docflow.visualizador.dominio;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "TB_ARQUIVO")
@SequenceGenerator(name = "seqArquivoGenerator", sequenceName = "SQ_ARQUIVO")
public class Arquivo implements Serializable {
	
	private static final long serialVersionUID = -5036712319323210445L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seqArquivoGenerator")
	@Column(name = "ID_ARQUIVO")
	private Integer id;

	@Column(name = "NM_ARQUIVO", length = 255, nullable = false)
	private String nome;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
	@JoinColumn(name = "ID_CONTEUDO_ARQUIVO")
	private ConteudoArquivo conteudoArquivo;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	@Transient
	public String getExtensao() {
		String extensao = "";
		int ultimoIndicePonto = getNome().lastIndexOf('.');
		if (ultimoIndicePonto > -1) {
			extensao = getNome().substring(ultimoIndicePonto + 1);
		}
		return extensao.toUpperCase();
	}

	public ConteudoArquivo getConteudoArquivo() {
		return conteudoArquivo;
	}

	public void setConteudoArquivo(ConteudoArquivo conteudoArquivo) {
		this.conteudoArquivo = conteudoArquivo;
	}
}