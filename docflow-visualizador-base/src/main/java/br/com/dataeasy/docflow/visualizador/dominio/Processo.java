package br.com.dataeasy.docflow.visualizador.dominio;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Table(name = "TB_PROCESSO")
@Entity
@SequenceGenerator(name = "seqProcessoGenerator", sequenceName = "SQ_PROCESSO")
public class Processo implements Serializable {
	
	private static final long serialVersionUID = -2430566698701506623L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "seqProcessoGenerator")
	@Column(name = "ID_PROCESSO")
	private Integer id;
	
	@Column(name = "DT_CRIACAO", nullable = false)
	private Date dataCriacao;
	
	@Column(name = "NM_PROCESSO", length = 100, nullable = false)
	private String nomeProcesso;
	
	@OneToMany(mappedBy = "processo")
	private List<Documento> documentos;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the dataCriacao
	 */
	public Date getDataCriacao() {
		return dataCriacao;
	}

	/**
	 * @param dataCriacao the dataCriacao to set
	 */
	public void setDataCriacao(Date dataCriacao) {
		this.dataCriacao = dataCriacao;
	}

	/**
	 * @return the nomeProcesso
	 */
	public String getNomeProcesso() {
		return nomeProcesso;
	}

	/**
	 * @param nomeProcesso the nomeProcesso to set
	 */
	public void setNomeProcesso(String nomeProcesso) {
		this.nomeProcesso = nomeProcesso;
	}

	/**
	 * @return the documentos
	 */
	public List<Documento> getDocumentos() {
		return documentos;
	}

	/**
	 * @param documentos the documentos to set
	 */
	public void setDocumentos(List<Documento> documentos) {
		this.documentos = documentos;
	}
	
	@PrePersist
	public void prePersist() {
		if (this.dataCriacao == null) {
			this.dataCriacao = new Date();
		}
	}
}
