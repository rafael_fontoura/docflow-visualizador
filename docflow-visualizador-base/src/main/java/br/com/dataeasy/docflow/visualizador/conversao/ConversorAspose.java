package br.com.dataeasy.docflow.visualizador.conversao;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import br.com.dataeasy.docflow.visualizador.dominio.Arquivo;
import br.com.dataeasy.docflow.visualizador.service.VisualizadorDocumentoService;
import br.com.dataeasy.docflow.visualizador.util.FileUtil;

import com.aspose.cells.FileFormatType;
import com.aspose.cells.Workbook;
import com.aspose.slides.Presentation;
import com.aspose.slides.PresentationEx;
import com.aspose.words.Document;

public abstract class ConversorAspose {
	
	@Resource
	private VisualizadorDocumentoService visualizadorDocumentoService;

	private final List<String> FORMATOS_CONVERSAO_SUPORTADOS = Arrays.asList("doc", "docx", "xls", "xlsx", "ppt",
			"pptx", "odt", "ods", "odp");

	protected abstract File converterParaPDF(Object... entradas);

	protected File converterWordParaPDF(Document doc, String caminhoCompletoDestino) throws Exception {
		String nomePDF = criarNomeDocumentoPDF(caminhoCompletoDestino);
		doc.save(nomePDF);
		return new File(nomePDF);
	}

	protected File converterExcelParaPDF(Workbook workbook, String caminhoCompletoDestino) throws Exception {
		String nomePDF = criarNomeDocumentoPDF(caminhoCompletoDestino);
		workbook.save(caminhoCompletoDestino, FileFormatType.PDF);
		return new File(nomePDF);
	}

	protected File converterPPTParaPDF(Presentation pres, String caminhoCompletoDestino) throws Exception {
		String nomePDF = criarNomeDocumentoPDF(caminhoCompletoDestino);
		pres.save(nomePDF, com.aspose.slides.SaveFormat.Pdf);
		return new File(nomePDF);
	}

	protected File converterPPTXParaPDF(PresentationEx pres, String caminhoCompletoDestino) throws Exception {
		String nomePDF = criarNomeDocumentoPDF(caminhoCompletoDestino);
		pres.save(nomePDF, com.aspose.slides.SaveFormat.Pdf);
		return new File(nomePDF);
	}

	protected boolean elementoExisteNaLista(String valorTeste, String... elementos) {
		return Arrays.asList(elementos).contains(valorTeste);
	}

	protected void validarFormato(String name) {
		String formato = ArquivoUtil.getExtensao(name);
		if (!FORMATOS_CONVERSAO_SUPORTADOS.contains(formato)) {
			throw new ConversaoNaoSuportadaExeption("Não é possível converter do formato " + formato + ".");
		}
	}

	protected String criarNomeDocumentoPDF(File origem) throws IOException {
		String caminho = origem.getCanonicalPath();
		return criarNomeDocumentoPDF(caminho);
	}

	protected String criarNomeDocumentoPDF(String caminho) {
		return caminho.substring(0, caminho.lastIndexOf(".")) + ".pdf";
	}

	public String criarNomeDocumentoPDF(File diretorioDestino, Arquivo arquivo) throws IOException {
		String md5 = FileUtil.calcularMD5Checksum(new ByteArrayInputStream(arquivo.getConteudoArquivo().getConteudo()));
		String nome = arquivo.getNome();
		String nomeSemExtensao = nome.substring(0, nome.lastIndexOf(".")) + md5;
		if (nomeSemExtensao.length() > 255) {
			nomeSemExtensao = nomeSemExtensao.substring(0, 255);
		}
		String nomePDF = nomeSemExtensao + ".pdf";
		nomePDF = visualizadorDocumentoService.formatarNomeDocumento(nomePDF);
		return new File(diretorioDestino, nomePDF).getCanonicalPath();
	}

	public boolean documentoPDFExiste(Arquivo arquivoOrigem, File diretorio) throws IOException {
		String caminhoDPF = criarNomeDocumentoPDF(diretorio, arquivoOrigem);
		return new File(caminhoDPF).exists();
	}

	public boolean permiteConversao(String extensao) {
		return FORMATOS_CONVERSAO_SUPORTADOS.contains(extensao);
	}
}