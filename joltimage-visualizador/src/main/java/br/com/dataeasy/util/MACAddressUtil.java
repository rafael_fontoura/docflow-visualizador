package br.com.dataeasy.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.logging.Logger;

public class MACAddressUtil {

	private static final Logger LOG = Logger.getLogger(MACAddressUtil.class.getName());

	public static InetAddress getMacAddress() {
		InetAddress ip;
		try {
			ip = InetAddress.getLocalHost();
			LOG.info("Current IP address : " + ip.getHostAddress());

			//NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
			while (networkInterfaces.hasMoreElements()) {
				NetworkInterface network = networkInterfaces.nextElement();
				byte[] mac = network.getHardwareAddress();
				
				StringBuilder sb = new StringBuilder();
				if (mac != null) {
					for (int i = 0; i < mac.length; i++) {
						sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? ":" : ""));
					}
					LOG.info("MAC address: " + sb.toString());
				}
			}

			return ip;
		} catch (UnknownHostException e) {
			throw new RuntimeException("Erro recuperando MAC address", e);
		} catch (SocketException e) {
			throw new RuntimeException("Erro recuperando MAC address", e);
		}
	}
}