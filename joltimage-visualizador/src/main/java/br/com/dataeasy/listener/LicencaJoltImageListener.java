package br.com.dataeasy.listener;

import java.io.File;
import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import br.com.dataeasy.util.MACAddressUtil;

import com.atalasoft.licensing.LicenseValidator;

public class LicencaJoltImageListener implements ServletContextListener {

	private static final Logger LOG = Logger.getLogger(LicencaJoltImageListener.class.getName());

	public void contextDestroyed(ServletContextEvent arg0) {
		// não faz nada
	}

	public void contextInitialized(ServletContextEvent sce) {
		MACAddressUtil.getMacAddress();
		ServletContext servletContext = sce.getServletContext();
		String nomeArquivo = servletContext.getInitParameter("arquivoLicencaJoltImage");
		String caminhoArquivo = "C:/bin/jboss-as-7.1.0.Final/dataeasy/docflow/" + nomeArquivo;
		File arquivo = new File(caminhoArquivo);
		LicenseValidator.validateLicense(arquivo);
		LOG.info("Licença JoltImage OK");
	}
}
