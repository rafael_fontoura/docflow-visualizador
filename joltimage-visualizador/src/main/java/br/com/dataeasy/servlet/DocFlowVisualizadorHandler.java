package br.com.dataeasy.servlet;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atalasoft.imaging.webcontrols.WebDocumentRequestHandler;

@MultipartConfig
@WebServlet(name = "VisualizadorHandler", urlPatterns = { "*.view" })
public class DocFlowVisualizadorHandler extends WebDocumentRequestHandler {

    private static final long serialVersionUID = -8983713753384758444L;
    // IP or name of center host
    private String            centerHostIPAndPort;
    private boolean           redirecionarProcessamento = false;

    public DocFlowVisualizadorHandler() {
        super();
    }

    private void preparar(HttpServletRequest request, HttpServletResponse response) {
        response.setHeader("Cache-Control", "private, no-store, no-cache, must-revalidate");
        response.setHeader("Pragma", "no-cache");
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // get center host IP and port to redirect the requests of image/PDF processing
        this.centerHostIPAndPort = "localhost:8080";
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        preparar(request, response);
        if (redirecionarProcessamento && isDifferentHostRequesting(request, response)) {
            redirectProcessing(request, response);
        } else {
            super.doPost(request, response);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        preparar(request, response);
        if (redirecionarProcessamento && isDifferentHostRequesting(request, response)) {
            redirectProcessing(request, response);
        } else {
            super.doGet(request, response);
        }
    }

    private void redirectProcessing(HttpServletRequest request, HttpServletResponse response) {
        String localHostNameAndPort = getLocalHostNameAndPort(request);
        String redirectURL = request.getRequestURL().toString().replace(localHostNameAndPort, centerHostIPAndPort);
        try {
            redirectURL = response.encodeRedirectURL(redirectURL + getQueryString(request));
            response.sendRedirect(redirectURL);
        } catch (IOException e) {
            throw new RuntimeException("Problema redirecionando processamento de imagem/PDF", e);
        }
    }

    private String getQueryString(HttpServletRequest request) {
        String queryString = request.getQueryString();
        if (queryString != null) {
            queryString = "?" + queryString;
        } else {
            queryString = "";
        }
        return queryString;
    }

    private String getLocalHostNameAndPort(HttpServletRequest request) {
        String localHostNameAndPort = request.getServerName();
        String requestURL = request.getRequestURL().toString();
        if (requestURL.charAt(requestURL.indexOf(localHostNameAndPort) + localHostNameAndPort.length()) == ':') {
            localHostNameAndPort += ":" + request.getServerPort();
        }
        return localHostNameAndPort;
    }

    private boolean isDifferentHostRequesting(HttpServletRequest request, HttpServletResponse response) {
        String port = ":" + request.getLocalPort();
        String serverNameAndPort = request.getServerName() + port;
        String serverIPAndPort = request.getLocalAddr() + port;

        String centerHostNameOrIPWithPort = centerHostIPAndPort;
        if (centerHostNameOrIPWithPort.indexOf(':') == -1) {
            centerHostNameOrIPWithPort += ":80";
        }

        return !serverNameAndPort.equals(centerHostNameOrIPWithPort) && !serverIPAndPort.equals(centerHostNameOrIPWithPort);
    }
}