package br.com.dataeasy.servlet;

import java.io.IOException;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atalasoft.imaging.webcontrols.WebDocumentRequestHandler;

@MultipartConfig
@WebServlet(name = "WebDocViewerHandler", urlPatterns = { "/WebDocViewerHandler" })
public class WebDocViewer extends WebDocumentRequestHandler {

	private static final long serialVersionUID = 4273714861391095423L;
	private boolean direcionarParaServidor = true;

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response) {
		
		if (direcionarParaServidor) {
			String servidor = "localhost:8080";
			String nomeServidor = request.getServerName();
			
			String nomeASubstituir = request.getServerName();
			String requestURL = request.getRequestURL().toString();
			if (requestURL.charAt(requestURL.indexOf(nomeServidor) + nomeServidor.length()) == ':') {
				nomeASubstituir += ":" + request.getServerPort();
			}
			
			String novaURL = response.encodeRedirectURL("http://localhost:8080/docflow/xhtml/docflow/principal/visualizar.view"); 
					//requestURL.replace(nomeASubstituir, servidor);
			direcionarRenderizacaoParaServidor(response, servidor, novaURL);
		} else {
			super.doGet(request, response);
		}
	}

	private void direcionarRenderizacaoParaServidor(HttpServletResponse response, String servidor, String novaURL) {
		try {
			response.sendRedirect(novaURL);
		} catch (IOException e) {
			throw new RuntimeException("Erro direcionando para servidor " + servidor + ".", e);
		}
	}
}