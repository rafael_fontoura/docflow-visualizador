<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="resources/WebDocViewer/atalaWebDocumentViewer.css" rel="Stylesheet" type="text/css" />
<script src="resources/WebDocViewer/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="resources/WebDocViewer/atalaWebDocumentViewer.js" type="text/javascript"></script>
</head>
<body>
	<form action="${pageContext.request.contextPath}/DownloadFileServlet" method="post">
		<input type="submit" name="button1" value="Download" />
	</form>
	<form>
		<table>
			<tr>
				<td>
					<div id="_toolbar1" class="atala-document-toolbar" style="width: 670px;"></div>
					<div id="_container1" class="atala-document-container" style="width: 670px; height: 500px;"></div>
				</td> 
			</tr>
		</table>
		<select id="arquivo" name="arquivo">
			<option value="15MB.pdf">PDF 1.5 MB - 32 p&aacute;ginas</option>
			<option value="17MB.pdf">PDF 1.7 MB - 1018 p&aacute;ginas</option>
			<option value="150KB.pdf">PDF 150 KB - 6 p&aacute;ginas</option>
			<option value="43MB.tif">TIF 4.3 MB - multipage_tif_example</option>
			<option value="DocFlow_RESTful_Web_Services_v12.pdf">DocFlow_RESTful_Web_Services_v12.pdf - 1.3MB - 75 p&aacute;ginas</option>
		</select>
		<button onclick="return abrir('/exemplos/' + document.getElementById('arquivo').value)">Carregar imagem</button>
		<script type="text/javascript" language="Javascript">
	        var _docUrl = '/exemplos/41350-vm.dff';
	        var _serverUrl = 'processImage.view';
	
	        try {
	            var _viewer = new Atalasoft.Controls.WebDocumentViewer({
	                'parent' : $('#_container1'), // parent container to put the viewer in
	                'toolbarparent' : $('#_toolbar1'), // parent container to put the viewer toolbar in
	                'serverurl' : _serverUrl, // server handler url to send image requests to
	                'documenturl' : _docUrl, // document url relative to the server handler url
	                'showbuttontext' : false
	            });
	        } catch (e) {
	            alert("Erro visualizando arquivo: " + e.message + ".");
	        }
	        
	        function abrir(caminhoImagem) {
	            _viewer.OpenUrl(caminhoImagem);
	            return false;
	        }
	    </script>
	</form>
</body>
</html>
